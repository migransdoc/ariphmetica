# загуглить назва армифметика
from time import time
from random import randint, choice
from functools import reduce


def check_time(start_time):
    if time() - start_time >= 60: return True
    return False


def generate_new_free_int_plus():  # генерируем новое рандомное число как свободный член
    return randint(25, 45)


def genetate_new_free_int_minus():
    return randint(40, 80)


def generate_new_int_division():
    return randint(40, 80)


def generate_new_int_multiplication():
    return randint(3, 9), randint(3, 9)


def factors(n):  # находим делители числа, чтобы выяснить, простое ли оно
    # если нет, то берем любой делитель
    return set(reduce(list.__add__,
                ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))


def random_divisor(n):  # выбирает случайный делитель числа
    div = factors(n)
    if len(div) == 2:
        return False
    div.discard(1)
    div.discard(n)
    return choice(list(div))


def main():
    start_time = time()  # starting time from which we count 1 minute for the game
    true_ans = false_ans = 0
    while True:
        first_num = second_num = third_num = 0
        operation1 = randint(0, 1)  # 0 is for multiplication, 1 is for division
        operation2 = randint(0, 1)  # 0 is for plus, 1 is for minus
        order = randint(0, 1)  # 0 is for "2 numbers before free number" , 1 otherwise

        while first_num == 0 or second_num == 0 or third_num == 0:
            if operation1:
                first_num = generate_new_int_division()
                second_num = random_divisor(first_num)
                if not second_num: continue
                res1 = str(first_num) + ' / ' + str(second_num)
            else:
                first_num, second_num = generate_new_int_multiplication()
                res1 = str(first_num) + ' * ' + str(second_num)

            if operation2:
                third_num = genetate_new_free_int_minus()
                res2 = ' - '
            else:
                third_num = generate_new_free_int_plus()
                res2 = ' + '

        if order:
            string = str(third_num) + res2 + res1 + '    '
            ans = int(input(string))
        else:
            string = res1 + res2 + str(third_num) + '    '
            ans = int(input())

        if ans == eval(string):
            true_ans += 1
        else:
            false_ans += 1

        if check_time(start_time):
            print(true_ans, false_ans)
            break


main()
